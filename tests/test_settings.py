import copy

import pytest

import conftest
import cpg.settings


@pytest.fixture
def settings():
    return cpg.settings.default()


def test_merge_empty_settings(settings):
    expected = copy.deepcopy(settings)
    cpg.settings.merge({}, settings)
    assert settings == expected


@pytest.mark.parametrize('key1, key2, input, expected',
                         conftest.settings_tuple())
def test_merge_valid_settings(key1, key2, input, expected, settings):
    d = cpg.settings.factory(key1, key2, input)
    orig_settings = copy.deepcopy(settings)
    cpg.settings.merge(d, settings)

    assert settings[key1][key2] == expected
    settings[key1][key2] = orig_settings[key1][key2]
    assert settings == orig_settings


@pytest.mark.parametrize('key1, key2, input, _',
                         conftest.settings_tuple(valid=False))
def test_merge_invalid_settings(key1, key2, input, _, settings):
    d = cpg.settings.factory(key1, key2, input)
    with pytest.raises(cpg.settings.InvalidSetting):
        cpg.settings.merge(d, settings)


@pytest.mark.parametrize('input, expected', conftest.valid_format_gen())
def test_set_format_valid_settings(input, expected, settings):
    cpg.settings.set_format(input, settings)
    assert settings['playlist']['format'] == expected


@pytest.mark.parametrize('format', conftest.invalid_format_gen())
def test_set_format_invalid_settings(format, settings):
    with pytest.raises(cpg.settings.InvalidSetting):
        cpg.settings.set_format(format, settings)


@pytest.mark.parametrize('input, expected', conftest.valid_paths_gen())
def test_set_paths_valid_settings(input, expected, settings):
    cpg.settings.set_paths(input, settings)
    assert settings['playlist']['paths'] == expected


@pytest.mark.parametrize('paths', conftest.invalid_paths_gen())
def test_set_paths_invalid_settings(paths, settings):
    with pytest.raises(cpg.settings.InvalidSetting):
        cpg.settings.set_paths(paths, settings)
