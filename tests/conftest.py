import io

import pytest


class FakeTrack:

    def __init__(self, filename):
        self.filename = filename


@pytest.fixture
def file():
    with io.StringIO() as f:
        yield f


@pytest.fixture(params=('m3u8', 'pls'), scope='session')
def format(request):
    return request.param


@pytest.fixture(params=('absolute', 'relative'), scope='session')
def paths(request):
    return request.param


def settings_tuple(valid=True):
    """Generate 4-tuples of two setting keys, an input and expected value.

    Generate valid or invalid settings depending on argument 'valid'.  Return
    tuple(key1, key2, input, expected).
    """
    for g in groups_gen(valid):
        yield ('groups', *g)
    for p in playlists_gen(valid):
        yield ('playlist', *p)
    for t in tracks_gen(valid):
        yield ('tracks', *t)


def groups_gen(valid):
    for fn in fallback_name_gen(valid):
        yield 'fallback_name', fn, fn
    for s in separate_gen(valid):
        yield 'separate', s, s
    for st in split_tags_gen(valid):
        yield 'split_tags', st, st
    for t in tags_gen(valid):
        yield 'tags', t, t


def playlists_gen(valid):
    if valid:
        for f in valid_format_gen():
            yield ('format', *f)
        for p in valid_paths_gen():
            yield ('paths', *p)
    else:
        for f in invalid_format_gen():
            yield 'format', f, f
        for p in invalid_paths_gen():
            yield 'paths', p, p


def tracks_gen(valid):
    for b in blacklist_gen(valid):
        yield 'blacklist', b, b
    for s in sort_gen(valid):
        yield 'sort', s, s


def fallback_name_gen(valid):
    if valid:
        yield from ('', 'name1', 'NAme2')
    else:
        yield from (1, True, None)


def separate_gen(valid):
    if valid:
        yield from (
            {},
            {'': []}, {'group': []},
            {'group': [{}]},
            {'group': [{'': ''}]}, {'group': [{'tag': 'val'}]},
            {'group': [
                {
                'tag1': 'val1',
                'TAG2': 'VAL2',
                }
            ]},
            {'group': [
                {
                    'tag1': 'val1',
                    'TAG2': 'VAL2',
                },
                {
                    'TAg3': 'vAL3',
                    'tAG4': 'VAl4',
                },
            ]},
        )
    else:
        yield from (
            '', [],
            {'group': ''}, {'group': {}},
            {1: []}, {None: []},
            {'group': [1]}, {'group': [None]},
            {'group': [{}, 1]}, {'group': [1, {}]},
            {'group': [{'tag': 1}]}, {'group': [{1: 'tag'}]},
            {'group': [
                {'tag': 'val'},
                {'tag': 1},
            ]},
            {'group': [
                {'tag': 'val'},
                {1: 'val'},
            ]},
        )


def split_tags_gen(valid):
    if valid:
        yield from (
            {},
            {'': []}, {'group': []},
            {'GROup': ['tag']},
            {'grOUP': ['TAg1', 'tAG2']},
            {
                'group1': ['TAG1', 'tag2'],
                'GROUP2': ['tag3', 'TAG4', 'tag5'],
            },
        )
    else:
        yield from (
            '', [],
            {'group': ''}, {'group': {}},
            {1: []}, {None: []},
            {'group': [1]}, {'group': [None]},
            {'group': ['tag1', 'tag2', True]},
        )


def tags_gen(valid):
    if valid:
        yield from ([], [[]], [['a']], [['b', 'c']])
    else:
        yield from (1, [1], [[1]], [['a', 1]])


def valid_format_gen():
    formats = ('m3u8', 'm3U8', 'M3U8', 'pls', 'pLs', 'PLS')
    yield from ((f, f.lower()) for f in formats)


def invalid_format_gen():
    yield from ('', 'invalid')


def valid_paths_gen():
    paths = ('absolute', 'ABSOlute', 'relative', 'relaTIVE')
    yield from ((p, p.lower()) for p in paths)


def invalid_paths_gen():
    yield from ('', 'invalid')


def blacklist_gen(valid):
    yield from separate_gen(valid)  # Identical structure.


def sort_gen(valid):
    yield from tags_gen(valid)  # Identical structure.
