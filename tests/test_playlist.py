import textwrap

import pytest

import conftest
import cpg.playlist
import cpg.settings


def test_write_playlist(file, groups, music_dir, settings, playlist):
    cpg.playlist.write(file, groups, music_dir, settings)
    assert file.getvalue() == playlist


def test_write_empty_playlist(file, empty_groups, music_dir, settings,
                              empty_playlist):
    cpg.playlist.write(file, empty_groups, music_dir, settings)
    assert file.getvalue() == empty_playlist


@pytest.fixture()
def groups():
    return [
        ('Group 1', [
            conftest.FakeTrack('/root/music/Group 1/1 - 01 - Track 1.flac'),
            conftest.FakeTrack('/root/music/Group 1/1 - 02 - Track 2.flac'),
            conftest.FakeTrack('/root/music/Group 1/2 - 01 - Track 3.flac'),
            conftest.FakeTrack('/root/music/Group 1/2 - 02 - Track 4.flac'),
        ]),
        ('Group 2', [
            conftest.FakeTrack('/root/music/Group 2/01 - Track 1.ogg'),
            conftest.FakeTrack('/root/music/Group 2/02 - Track 2.ogg'),
        ]),
        ('Group 3', [
            conftest.FakeTrack('/root/music/Group 3/01 - Track 1.mp3'),
        ]),
    ]


def empty_groups_gen():
    yield from (
        [],
        [
            ('Group 1', []),
            ('Group 2', []),
        ]
    )


@pytest.fixture(params=empty_groups_gen())
def empty_groups(request):
    return request.param


@pytest.fixture(params=('/root/music', '//root//music///'), scope='session')
def music_dir(request):
    return request.param


@pytest.fixture
def settings(format, paths):
    setts = cpg.settings.default()
    cpg.settings.set_format(format, setts)
    cpg.settings.set_paths(paths, setts)
    return setts


@pytest.fixture(scope='session')
def playlist(format, paths):
    if format == 'm3u8':
        return m3u8_playlist(paths)
    if format == 'pls':
        return pls_playlist(paths)
    assert False


@pytest.fixture(scope='session')
def empty_playlist(format):
    if format == 'm3u8':
        return ''
    if format == 'pls':
        return textwrap.dedent(
            """\
            [playlist]
            NumberOfEntries=0
            Version=2

            """
        )
    assert False


def m3u8_playlist(paths):
    if paths == 'absolute':
        return textwrap.dedent(
            """\
            /root/music/Group 1/1 - 01 - Track 1.flac
            /root/music/Group 1/1 - 02 - Track 2.flac
            /root/music/Group 1/2 - 01 - Track 3.flac
            /root/music/Group 1/2 - 02 - Track 4.flac
            /root/music/Group 2/01 - Track 1.ogg
            /root/music/Group 2/02 - Track 2.ogg
            /root/music/Group 3/01 - Track 1.mp3
            """
        )
    if paths == 'relative':
        return textwrap.dedent(
            """\
            Group 1/1 - 01 - Track 1.flac
            Group 1/1 - 02 - Track 2.flac
            Group 1/2 - 01 - Track 3.flac
            Group 1/2 - 02 - Track 4.flac
            Group 2/01 - Track 1.ogg
            Group 2/02 - Track 2.ogg
            Group 3/01 - Track 1.mp3
            """
        )
    assert False


def pls_playlist(paths):
    if paths == 'absolute':
        return textwrap.dedent(
            """\
            [playlist]
            File1=/root/music/Group 1/1 - 01 - Track 1.flac
            File2=/root/music/Group 1/1 - 02 - Track 2.flac
            File3=/root/music/Group 1/2 - 01 - Track 3.flac
            File4=/root/music/Group 1/2 - 02 - Track 4.flac
            File5=/root/music/Group 2/01 - Track 1.ogg
            File6=/root/music/Group 2/02 - Track 2.ogg
            File7=/root/music/Group 3/01 - Track 1.mp3
            NumberOfEntries=7
            Version=2

            """
        )
    if paths == 'relative':
        return textwrap.dedent(
            """\
            [playlist]
            File1=Group 1/1 - 01 - Track 1.flac
            File2=Group 1/1 - 02 - Track 2.flac
            File3=Group 1/2 - 01 - Track 3.flac
            File4=Group 1/2 - 02 - Track 4.flac
            File5=Group 2/01 - Track 1.ogg
            File6=Group 2/02 - Track 2.ogg
            File7=Group 3/01 - Track 1.mp3
            NumberOfEntries=7
            Version=2

            """
        )
    assert False
