Welcome to CPG's Documentation!
===============================

CPG (stands for configurable playlist generator) is a command line application
for recursively scanning a directory for audio files and outputing a playlist
file.  Several ways of influencing the ordering of tracks in the generated
playlist are available.  The generated playlist's file format and the type of
its paths (absolute or relative) can be configured too.

The source code of the application, tests and documentation is available at
CPG's `GitLab`_ repository.  Please refer to pages linked in the contents for
details about installation and usage.

.. _GitLab: https://gitlab.com/9f/cpg

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage

* :ref:`search`
